package com.example.a19900706_mariorenemartinezquintal_nycschools.schools.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.a19900706_mariorenemartinezquintal_nycschools.SAT_ListViewHolder
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.repository.SAT_Repository
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.repository.SchoolRepository

class VMFactorySAT(private val repository: SAT_Repository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(SAT_ListViewModel::class.java)) {
            SAT_ListViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }    }
}
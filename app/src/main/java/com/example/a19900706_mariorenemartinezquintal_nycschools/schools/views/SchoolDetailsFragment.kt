package com.example.a19900706_mariorenemartinezquintal_nycschools.schools.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.a19900706_mariorenemartinezquintal_nycschools.SAT_Adapter
import com.example.a19900706_mariorenemartinezquintal_nycschools.databinding.FragmentSchoolDetailsBinding
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.models.School
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.repository.SAT_Repository
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.viewmodel.SAT_ListViewModel
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.viewmodel.VMFactorySAT


class SchoolDetailsFragment : Fragment() {
    private lateinit var binding: FragmentSchoolDetailsBinding
    val viewModel by viewModels<SAT_ListViewModel> {
        VMFactorySAT(SAT_Repository())

    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentSchoolDetailsBinding.inflate(inflater,container,false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dbn: String? = arguments?.getString("school")
        //val platforms =arguments?.getSerializable("school") as School?
        dbn?.let{
        viewModel.fetchSAT_List(dbn)
        }
        viewModel.satListLiveData.observe(viewLifecycleOwner, Observer {
            if(it.isEmpty()){
                Toast.makeText(requireContext(),"no data for this school", Toast.LENGTH_LONG).show()
            }
            else {
                binding.recyclerView.adapter = SAT_Adapter(it)
            }

        })



    }
}
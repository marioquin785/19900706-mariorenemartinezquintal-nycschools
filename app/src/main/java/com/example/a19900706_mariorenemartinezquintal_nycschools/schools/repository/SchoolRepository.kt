package com.example.a19900706_mariorenemartinezquintal_nycschools.schools.repository

import com.example.a19900706_mariorenemartinezquintal_nycschools.network.SAT_Api
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.models.School
import com.example.a19900706_mariorenemartinezquintal_nycschools.network.SchoolsListApi
import com.example.a19900706_mariorenemartinezquintal_nycschools.network.SchoolsRetrofitHelper
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.models.SAT

class SchoolRepository() {

    val schoolsListApi = SchoolsRetrofitHelper
        .getInstance()
        .create(SchoolsListApi::class.java)


    suspend fun getSchoolsList(): List<School> {
        val listResponse = schoolsListApi.getSchoolsList()
        return listResponse
    }


}

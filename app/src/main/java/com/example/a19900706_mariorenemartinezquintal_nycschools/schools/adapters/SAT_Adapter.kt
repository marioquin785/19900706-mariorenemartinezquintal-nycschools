package com.example.a19900706_mariorenemartinezquintal_nycschools

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a19900706_mariorenemartinezquintal_nycschools.databinding.FragmentSatResultsListBinding
import com.example.a19900706_mariorenemartinezquintal_nycschools.databinding.SchoolListItemBinding
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.models.SAT
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.views.SatResultsListFragment

class SAT_Adapter(
    private var SAT_List: List<SAT>
) : RecyclerView.Adapter<SAT_ListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SAT_ListViewHolder {
        val binding = FragmentSatResultsListBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return SAT_ListViewHolder(binding)
    }


    override fun onBindViewHolder(holder: SAT_ListViewHolder, position: Int) {
        with(holder) {
            with(SAT_List[position]) {

                binding.shoolName.text = this.schoolName
                binding.takersLabel.text =  binding.takersLabel.text.toString()
                binding.tvTestTakers.text = this.numOfSatTestTakers
                binding.tvReadingAvScore.text = this.satCriticalReadingAvgScore
                binding.tvMathAvScore.text = this.satMathAvgScore
                binding.tvWritingScore.text = this.satWritingAvgScore


            }
        }
    }


    override fun getItemCount(): Int {
        return SAT_List.size
    }
}

class SAT_ListViewHolder(
    val binding: FragmentSatResultsListBinding
) : RecyclerView.ViewHolder(binding.root)

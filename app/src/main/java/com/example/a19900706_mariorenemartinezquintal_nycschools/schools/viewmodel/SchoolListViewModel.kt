package com.example.a19900706_mariorenemartinezquintal_nycschools.schools.viewmodel

import androidx.lifecycle.*
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.repository.SchoolRepository
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.models.School
import kotlinx.coroutines.launch

class SchoolListViewModel(private val repository: SchoolRepository):ViewModel() {
    val schoolListLiveData = MutableLiveData<List<School>>()



       fun fetchSchoolsList() = viewModelScope.launch {
         val schoolsList =  repository.getSchoolsList()
               schoolListLiveData.postValue(schoolsList)
       }

}

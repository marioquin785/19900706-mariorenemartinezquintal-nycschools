package com.example.a19900706_mariorenemartinezquintal_nycschools.schools.viewmodel

import androidx.lifecycle.*
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.models.SAT
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.repository.SchoolRepository
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.models.School
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.repository.SAT_Repository
import kotlinx.coroutines.launch

class SAT_ListViewModel(private val repository: SAT_Repository) : ViewModel() {
    val satListLiveData = MutableLiveData<List<SAT>>()


    fun fetchSAT_List(dbn : String) = viewModelScope.launch {
        val SAT_ListViewModelList = repository.getSAT_List(dbn)
        satListLiveData.postValue(SAT_ListViewModelList)
    }

}

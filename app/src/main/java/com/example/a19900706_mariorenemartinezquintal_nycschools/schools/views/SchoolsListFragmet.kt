package com.example.a19900706_mariorenemartinezquintal_nycschools.schools.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.example.a19900706_mariorenemartinezquintal_nycschools.R
import com.example.a19900706_mariorenemartinezquintal_nycschools.SchoolsListAdapter
import com.example.a19900706_mariorenemartinezquintal_nycschools.databinding.FragmentSchoolListBinding
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.repository.SchoolRepository
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.viewmodel.SchoolListViewModel
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.viewmodel.VMFactory
import java.io.Serializable


class SchoolsListFragmet : Fragment() {
    private val TAG = "MAIN_ACTIVITY"
    private lateinit var binding: FragmentSchoolListBinding

    val viewModel by viewModels<SchoolListViewModel> {
        VMFactory(SchoolRepository())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSchoolListBinding.inflate(inflater,container,false)
       // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchSchoolsList()
        viewModel.schoolListLiveData.observe(viewLifecycleOwner, Observer { it ->
            binding.recyclerview.adapter = SchoolsListAdapter(it){ school->
                  val bundle = Bundle()
               bundle.putString("school", school.dbn  )
              //  bundle.putSerializable("school", school as Serializable  )
                this.findNavController().navigate(R.id.action_schoolListFragmet_to_schoolDetailsFragment2,bundle);
            }


        })




       }



}
package com.example.a19900706_mariorenemartinezquintal_nycschools.schools.repository

import com.example.a19900706_mariorenemartinezquintal_nycschools.network.SAT_Api
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.models.School
import com.example.a19900706_mariorenemartinezquintal_nycschools.network.SchoolsListApi
import com.example.a19900706_mariorenemartinezquintal_nycschools.network.SchoolsRetrofitHelper
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.models.SAT

class SAT_Repository() {

    val SAT_ListApi = SchoolsRetrofitHelper
        .getInstance()
        .create(SAT_Api::class.java)

    suspend fun getSAT_List(dbn: String): List<SAT> {
        val listResponse = SAT_ListApi.getSATList(dbn)
        return listResponse
    }


}

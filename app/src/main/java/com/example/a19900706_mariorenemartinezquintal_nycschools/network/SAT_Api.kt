package com.example.a19900706_mariorenemartinezquintal_nycschools.network

import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.models.SAT
import com.example.a19900706_mariorenemartinezquintal_nycschools.schools.models.School
import retrofit2.http.GET
import retrofit2.http.Query as Query1

interface SAT_Api {
    @GET("resource/f9bf-2cp4.json")
    suspend fun getSATList(@Query1("dbn") dbn: String): List<SAT>
}